# Electronic design

The electronic design is split in two boards, the Core and the Mega boards. The Core board
has the more challenging components (SoC, RAM, Gigabit switch, etc) with is very high density
in components. The Mega board is the expansion board with the power sources, ethernet ports, mPCIe
connectors, etc. 

Having the design split this way allow simpler customization and local manufacture of the Mega board.
Changes to the Mega board are simple, the board is only two layers and is not very dense. If your project
or community need some specific components then only modifying the Mega board is easy.

The development work of new features is currently being done in Gitlab. If you want to participate please
contact us!

## Core board

This board was originally developed using the Cadence 16.5 suite because it was based on a reference
design provided in this suite.

* PCB: Allegro
* SCH: OrCAD

The design files are provided in the original_project folder.
A free but propietary viewer and plotter can be downloaded from Cadence after registration:
* Allegro/OrCAD FREE Physical Viewers 16.6 - 15MB

### Migration to Altium and Kicad (Work in progress)

In order to migrate the project to Free Software we are working in migrating to Altium as a step
to migrate later to Kicad. 
For the time being the migration files are provided as sample only, we don't guarantee yet that 
this projects will generate correct gerbers (use the Allegro version for that).
If in the future there is need to modify the Core board then it will be evaluated to invest the time to audit
the migration and work directly from Kicad. If you want to work on this it will be appreciated by
the community.

## Mega board

The Mega board currently a Kicad project.
The first designs were made with Eagle. You can find the files at the corresponding tags.
After the first design we migrated the project to Kicad and it is now the reference design.

